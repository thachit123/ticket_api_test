# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import datetime
from mongoengine import Document, StringField, BooleanField, DateTimeField
from werkzeug.security import generate_password_hash

class User(Document):
    username = StringField(max_length=32, required=True, unique=True)
    password = StringField(min_length=6, max_length=100, required=True)
    first_name = StringField(max_length=32, required=True)
    last_name = StringField(max_length=32, required=True)
    active = BooleanField(default=True)
    created_date = DateTimeField(default=datetime.datetime.now())
    last_login = DateTimeField()

    def save(self, *args, **kwargs):
        if self._created or 'password' in self._changed_fields:
            self.password = generate_password_hash(self.password)

        return super(User, self).save(args, kwargs)
