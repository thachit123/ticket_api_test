# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

from flask import Blueprint, make_response
import json

home = Blueprint('home', __name__)


@home.route('/')
def index():
    res_data = {
        'name': 'Ticket Api System',
        'version': '0.1',
        'status': 'OK',
    }

    json_data = json.dumps(res_data)
    response = make_response(json_data)
    return response
