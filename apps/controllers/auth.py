# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

import json
from flask import Blueprint, request, make_response
from apps.models.user import User as UserModel
from apps.validators.user import User as UserSchema
auth = Blueprint('auth', __name__)


@auth.route('/api/v1/user/login', methods=['POST'])
def login():
    return "Login Controller"


@auth.route('/api/v1/user/create', methods=['POST'])
def create_user():
    try:
        form_data = request.json
        create_user_schema = UserSchema()
        errors = create_user_schema.validate(form_data)
        if errors:
            response = make_response(json.dumps({'error': errors}))
            response.status_code = 500
        else:
            user_obj = UserModel(
                username=form_data.get('username'),
                password=form_data.get('password'),
                first_name=form_data.get('first_name'),
                last_name=form_data.get('last_name')
            )
            user_obj.save()
            response = make_response(user_obj.to_json())
            response.status_code = 201

    except Exception as exc:
        response = make_response(json.dumps({'error': exc.args[0]}))
        response.status_code = 500

    response.content_type = 'application/json; charset=utf-8'
    return response


