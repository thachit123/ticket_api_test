# -*- coding: utf-8 -*-
__author__ = 'Thach Nguyen'

from marshmallow import Schema, fields
from marshmallow.validate import Length

class User(Schema):
    username = fields.Str(required=True, validate=Length(min=1, max=32))
    password = fields.Str(required=True, validate=Length(min=6, max=100))
    first_name = fields.Str(required=True, validate=Length(max=32))
    last_name = fields.Str(required=True, validate=Length(max=32))